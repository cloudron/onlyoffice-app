[1.0.0]
* Initial version

[1.0.1]
* Update OnlyOffice to 5.4.2

[1.1.0]
* Update OnlyOffice to 5.5.0

[1.1.1]
* Update OnlyOffice to 5.5.1

[1.2.0]
* Update to new Cloudron base image v2

[1.2.1]
* Update OnlyOffice to 5.5.3

[1.3.0]
* Update OnlyOffice to 5.6.0
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#560)

[1.3.1]
* Update OnlyOffice to 5.6.1
* Fix Path Traversal vulnerability via Convert Service param (Bug #45976)

[1.3.2]
* Update OnlyOffice to 5.6.2
* Fix Path Traversal vulnerability via savefile param (Bug #46037)

[1.3.3]
* Update OnlyOffice to 5.6.3
* Fix Path Traversal vulnerability via image upload params (Bug #46113)

[1.3.4]
* Update OnlyOffice to 5.6.4
* Fix several vulnerabilities in x2t (Bug #46348, Bug #46352, Bug #46353, Bug #46384, Bug #46434, Bug #46436)
* Fix vulnerability in TXT converter (Bug #46437)

[1.4.0]
* Update OnlyOffice to 5.6.5
* https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#565
* document-server-integration - Uses Apache license instead of MIT
* Fix SIGABR on ODT color (Bug #46499)(DocumentServer#989)

[1.5.0]
* Update OnlyOffice to 6.0.0
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#600)

[1.5.1]
* Rename to ONLYOFFICE Docs
* Update to 6.0.1
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#601)

[1.5.2]
* Update OnlyOffice to 6.0.2
* Fix problem with insert BMP image in doc (Bug #47276)

[1.6.0]
* Update OnlyOffice to 6.1.0

[1.6.1]
* Restrict rabbitmq scheduler count to 1

[1.6.2]
* Update OnlyOffice to 6.1.1
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#611)

[1.6.3]
* Update base image to version 3

[1.7.0]
* UPdate OnlyOffice to 6.2.0
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#620)

[1.7.1]
* Update OnlyOffice to 6.2.1
* Fix data loss after opening document reviewed by user (Bug #48920) (DocumentServer#1159)
* Fix JS error while entering text after cursor mouse move (Bug #49090)
* Fix opening docx file with chart in MS Office (Bug #49219)
* Fix copy from Document to Spreadsheet (Bug #49013)
* Fix missing plugin tab (Bug #49007)

[1.7.2]
* Update OnlyOffice to 6.2.2
* Remove ability to execute DocumentBuilder scripts from Editors
* Fix vulnerability with 'insert image from url' and 'compare document from url`
* Fix convert time for specific document (Bug #49434)
* Fix broken pptx file after open in Presentation Editor (Bug #49429, #49202)

[1.8.0]
* Update OnlyOffice to 6.3.0
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#630)

[1.8.1]
* Update OnlyOffice to 6.3.1
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#631)

[1.9.0]
* Update OnlyOffice to 6.4.0
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#640)

[1.9.1]
* Update OnlyOffice to 6.4.1
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#641)

[1.9.2]
* Update OnlyOffice to 6.4.2
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#642)

[1.9.3]
* Lower thread pool size to ensure reliable startup

[1.9.4]
* Fix spellchecker

[1.9.5]
* Support custom fonts

[1.10.0]
* Update OnlyOffice to 7.0.0
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#700)

[1.10.1]
* Update to base image 3.2.0

[1.10.2]
* Update OnlyOffice to 7.0.1
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#701)

[1.11.0]
* Update OnlyOffice to 7.1.0
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#710)

[1.11.1]
* Update OnlyOffice to 7.1.1
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#711)

[1.12.0]
* Update OnlyOffice to 7.2.0
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#720)

[1.12.1]
* Update OnlyOffice to 7.2.1
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#721)

[1.12.2]
* Update OnlyOffice to 7.2.2
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#722)

[1.13.0]
* Update OnlyOffice to 7.3.0
* Update Cloudron base image to 4.0
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#730)

[1.13.1]
* Update OnlyOffice to 7.3.2
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#732)

[1.13.2]
* Update OnlyOffice to 7.3.3
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#733)

[1.14.0]
* Update OnlyOffice to 7.4.0
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#740)

[1.14.1]
* Update OnlyOffice to 7.4.1
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#741)

[1.14.2]
* Update OnlyOffice to 7.4.1
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#741)

[1.15.0]
* Update OnlyOffice to 7.5.0
* Update base image to 4.2.0
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#750)

[1.15.1]
* Update OnlyOffice to 7.5.1
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#751)

[1.16.0]
* Update OnlyOffice to 8.0.0
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#800)

[1.16.1]
* Update OnlyOffice to 8.0.1
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#801)

[1.16.2]
* Fix custom font usage

[1.17.0]
* Update OnlyOffice to 8.1.0
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#810)

[1.17.1]
* Update OnlyOffice to 8.1.1
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#811)

[1.17.2]
* Update OnlyOffice to 8.1.3
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#813)
* Fixed bugs with cursor and interface objects positioning in Chromium browsers

[1.18.0]
* Update OnlyOffice to 8.2.0
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#820)
* Enable WOPI protocol

[1.18.1]
* Enable WOPI for updated instances also

[1.18.2]
* Update DocumentServer to 8.2.1
* [Full Changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#820)
* Fixed appearance of an extra argument when switching from a linear equation
* Fixed navigation via menu instead of moving around text when using
* Fixed inconsistency in the appearance of labels as compared to other editors
* Fixed the ability to add some `TIF`/`TIFF` images to documents
* Fixed an issue with calculating spacing before for a paragraph
* Fixed an issue with cell selection after removing table rows
* Fixed calculating footnotes in the extreme case when there is no space even
* Fixed an issue with the exact row height support when opening `DOCX` files
* ...

[1.18.3]
* Update DocumentServer to 8.2.2
* [Full Changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#820)
* Fixed the ability to read out a document in the Firefox browser using
* Fixed displaying some chart types when opening the `DOCX` or `PPTX` documents
* Fixed availability of buttons on the right panel when the zoom is higher
* Fixed stopping work of the editor when working with the Text box
* Fixed slow scrolling of documents if the document extends the visible area
* Fixed stopping work of the editor when comparing some `DOCX` documents

[1.19.0]
* Update DocumentServer to 8.3.0
* [Full Changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#820)
* Added support for logical operations on shapes (unite, combine, fragment,
* Added the Opacity option for images
* Added the ability to reset Crop for images in the right panel
* Added the interface translation into Albanian (sq-AL, Albanian (Albania))
* Added new languages to the list of exceptions for text AutoCorrect
* Implemented RTL interface support for the embedded viewer
* Blocking dialogs that require the editor restart are replaced

[1.19.1]
* Update DocumentServer to 8.3.1
* [Full Changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#820)
* Added the ability to move pages in `PDF` files ([DocumentServer#1749](https://github.com/ONLYOFFICE/DocumentServer/issues/3052))
* Added the parameter which hides tips about new features
* Fixed stopping work of the editor when pasting a large amount of copied data
* Fixed crash after entering the minus sign after the equation
* Fixed crash after entering a character before the division sign
* Fixed stopping work of the editor when building some `DOCX` files where
* Fixed an issue with undone changes when turning off and on the "Show changes
* Fixed crash of the editor when pasting text in the Track Changes mode in some
* Fixed stopping work of the editor when removing a paragraph
* Fixed reset of the Text direction > RTL option when placing the cursor

[1.19.2]
* Update DocumentServer to 8.3.2
* [Full Changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#832)
* Fixed crash when pasting content into a document in NoHistory mode
* Fixed stopping work of the editor when exiting the header/footer editing mode in some scenarios
* Fixed an error when processing comments and data when opening some DOCX files
* Fixed an issue with calculation of a float shape position in the header
* Fixed navigation using the Arrow Left / Arrow Right keys in Arabic text (DocumentServer#3114)
* Fixed the cursor position when entering LTR text in a RTL paragraph
* Fixed calculation of the cursor position for the last line of a RTL paragraph
* Fixed the display of margins for a RTL paragraph
* Fixed alignment for a RTL paragraph in some edge cases
* Fixed the order of text wrapping ranges for RTL paragraphs

[1.20.0]
* Update base image to 5.0.0

