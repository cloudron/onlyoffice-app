### Overview

ONLYOFFICE provides you with the most secure way to create, edit and collaborate on business documents online.
It is 100% compatible with MS Office formats.

Use this together with Nextcloud to build a powerfull collaboration suite or integrate with many other [3rdparty solutions](https://www.onlyoffice.com/all-connectors.aspx)
