FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN mkdir -p /app/code
WORKDIR /app/code

RUN useradd ds

RUN apt-get update && apt-get install -y nginx-extras rabbitmq-server ttf-mscorefonts-installer && rm -r /var/cache/apt /var/lib/apt/lists

# renovate: datasource=github-releases depName=ONLYOFFICE/DocumentServer versioning=semver extractVersion=^v(?<version>.+)$
ARG ODS_VERSION=8.3.2

RUN wget --quiet https://github.com/ONLYOFFICE/DocumentServer/releases/download/v${ODS_VERSION}/onlyoffice-documentserver_amd64.deb && \
    dpkg --unpack onlyoffice-documentserver_amd64.deb && \
    rm -f /var/lib/dpkg/info/onlyoffice-documentserver.postinst && \
    apt-get install -yfv && \
    rm -f onlyoffice-documentserver_amd64.deb

# generate allfonts.js and thumbnail
RUN mkdir -p /var/www/onlyoffice/documentserver/fonts

# first "true" argument is required to skip supervisor calls at the end of the script
RUN /usr/bin/documentserver-generate-allfonts.sh true

RUN ln -s /app/data/onlyoffice/lib /var/lib/onlyoffice && \
    rm -rf /etc/onlyoffice/documentserver/production-linux.json && ln -s /app/data/config/production-linux.json /etc/onlyoffice/documentserver/production-linux.json && \
    rm -rf /var/log/nginx/ && ln -s /tmp/logs/nginx /var/log/nginx && \
    rm -rf /var/lib/nginx/ && ln -s /run/lib/nginx /var/lib/nginx && \
    rm -rf /var/lib/rabbitmq/ && ln -s /app/data/rabbitmq /var/lib/rabbitmq && \
    rm -rf /var/log/rabbitmq/ && ln -s /tmp/rabbitmq /var/log/rabbitmq

# support custom fonts
RUN rm -rf /usr/local/share/fonts && ln -s /app/data/fonts /usr/local/share/fonts && \
    rm -rf /var/www/onlyoffice/documentserver/fonts && ln -s /run/onlyoffice/fonts /var/www/onlyoffice/documentserver/fonts && \
    rm -rf /var/www/onlyoffice/documentserver/sdkjs/common/AllFonts.js && ln -s /run/onlyoffice/AllWebFonts.js /var/www/onlyoffice/documentserver/sdkjs/common/AllFonts.js && \
    rm -rf /var/www/onlyoffice/documentserver/server/FileConverter/bin/AllFonts.js && ln -s /run/onlyoffice/AllFonts.js /var/www/onlyoffice/documentserver/server/FileConverter/bin/AllFonts.js && \
    rm -rf /var/www/onlyoffice/documentserver/server/FileConverter/bin/font_selection.bin && ln -s /run/onlyoffice/font_selection.bin /var/www/onlyoffice/documentserver/server/FileConverter/bin/font_selection.bin

RUN find /var/www/onlyoffice/documentserver/sdkjs/common/Images -name "fonts_thumbnail*.*" | while read fontthumbnail; do filename=$(basename $fontthumbnail); rm -f "$fontthumbnail"; ln -s "/run/onlyoffice/images/$filename" "$fontthumbnail"; done

# Ensure nginx is not running as daemon for supervisor
RUN sed -i '1s;^;daemon off\;\n;' /etc/nginx/nginx.conf

# Use and patch up nginx configs from the package
RUN rm -f /etc/nginx/sites-enabled/*
RUN mv /etc/onlyoffice/documentserver/nginx/ds.conf.tmpl.dpkg-new /etc/nginx/sites-enabled/ds.conf && \
    mv /etc/onlyoffice/documentserver/nginx/includes/http-common.conf.dpkg-new /etc/onlyoffice/documentserver/nginx/includes/http-common.conf && \
    mv /etc/onlyoffice/documentserver/nginx/includes/ds-mime.types.conf.dpkg-new /etc/onlyoffice/documentserver/nginx/includes/ds-mime.types.conf && \
    mv /etc/onlyoffice/documentserver/nginx/includes/ds-common.conf.dpkg-new /etc/onlyoffice/documentserver/nginx/includes/ds-common.conf && \
    mv /etc/onlyoffice/documentserver/nginx/includes/ds-docservice.conf.dpkg-new /etc/onlyoffice/documentserver/nginx/includes/ds-docservice.conf && \
    rm /etc/nginx/includes/ds-letsencrypt.conf && \
    rm /etc/nginx/includes/ds-example.conf
RUN sed -e "s/access_log off;/access_log \/dev\/stdout;/g" -e "s/error_log .*/error_log \/dev\/stdout info;/g" -i /etc/onlyoffice/documentserver/nginx/includes/ds-common.conf

# app moved to systemd unit files, so check deb package for new ones from time to time
COPY supervisor/* /etc/supervisor/conf.d/

# setup supervisor controlled processes to stdout logging
RUN crudini --set /etc/supervisor/supervisord.conf "supervisord" logfile "/dev/stdout" && \
    crudini --set /etc/supervisor/supervisord.conf "supervisord" logfile_maxbytes 0 && \
    crudini --set /etc/supervisor/supervisord.conf "supervisord" childlogdir "/tmp"

# enable default configs
RUN mv /etc/onlyoffice/documentserver/log4js/production.json.dpkg-new /etc/onlyoffice/documentserver/log4js/production.json && \
    mv /etc/onlyoffice/documentserver/default.json.dpkg-new /etc/onlyoffice/documentserver/default.json

# https://github.com/ONLYOFFICE/Docker-DocumentServer/blob/0052a315837ac68aec19d65cd41d1be0b243acb8/run-document-server.sh#L767C1-L768C53
# Fix to resolve the `unknown "cache_tag" variable` error
RUN documentserver-flush-cache.sh -r false

COPY start.sh production-linux.json patch-config.js /app/pkg/

CMD [ "/app/pkg/start.sh" ]
