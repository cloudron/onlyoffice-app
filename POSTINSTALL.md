This app has to be setup alongside NextCloud to provide a document editor.

In your NextCloud installation, install the OnlyOffice app and configure it
to use this app's domain and use `changeme` as the secret key.

**Please change the default secret in `/app/data/config/production-linux.json`.
Note that there are two places in the file where you need to change the secret.**
