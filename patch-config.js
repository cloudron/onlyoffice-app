
var fs = require('fs');

var CONFIG_FILE_PATH = '/app/data/config/production-linux.json';
var config;

console.log('Reading config from ' + CONFIG_FILE_PATH + ' ...');
try {
    config = JSON.parse(fs.readFileSync(CONFIG_FILE_PATH, 'utf-8'));
} catch (e) {
    console.error('Unable to patch config. Cannot continue.');
    process.exit(1);
}

console.log(config);

console.log('Patching Postgres credentials ...');
config.services.CoAuthoring.sql.dbHost = process.env.CLOUDRON_POSTGRESQL_HOST;
config.services.CoAuthoring.sql.dbPort = parseInt(process.env.CLOUDRON_POSTGRESQL_PORT, 10);
config.services.CoAuthoring.sql.dbName = process.env.CLOUDRON_POSTGRESQL_DATABASE;
config.services.CoAuthoring.sql.dbUser = process.env.CLOUDRON_POSTGRESQL_USERNAME;
config.services.CoAuthoring.sql.dbPass = process.env.CLOUDRON_POSTGRESQL_PASSWORD;

console.log('Patching Redis credentials ...');
config.services.CoAuthoring.redis.host = process.env.CLOUDRON_REDIS_HOST;
config.services.CoAuthoring.redis.port = parseInt(process.env.CLOUDRON_REDIS_PORT, 10);

console.log('Ensuring spellcheck dictionaries ...');
if (!config.SpellChecker) config.SpellChecker = {};
if (!config.SpellChecker.server) config.SpellChecker.server = {};
config.SpellChecker.server.dictDir = '/var/www/onlyoffice/documentserver/dictionaries';

console.log('Enabling wopi ...');
if (!config.wopi) config.wopi = {};
config.wopi.enable = true;

console.log('Writing config to ' + CONFIG_FILE_PATH + ' ...');
fs.writeFileSync(CONFIG_FILE_PATH, JSON.stringify(config, null, 2), 'utf-8');

console.log('Done');