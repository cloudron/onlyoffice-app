#!/bin/bash

set -eu

echo "=> Setup directories"
mkdir -p /app/data/{fonts,config,rabbitmq} /app/data/onlyoffice/lib
mkdir -p /tmp/logs/nginx /tmp/rabbitmq /run/lib/nginx /run/onlyoffice/{fonts,images}

if [[ ! -f /app/data/config/production-linux.json ]]; then
    echo "=> Install first time production-linux.json"
    cp /app/pkg/production-linux.json /app/data/config/production-linux.json
fi

echo "=> Patch configuration files"
node /app/pkg/patch-config.js

echo "=> Clear fonts cache"
rm -rf /run/onlyoffice/images && mkdir -p /run/onlyoffice/images

DIR="/var/www/onlyoffice/documentserver"

# Copied from /usr/bin/documentserver-generate-allfonts.sh
echo "=> Collect fonts"
LD_LIBRARY_PATH=/var/www/onlyoffice/documentserver/server/FileConverter/bin/ "$DIR/server/tools/allfontsgen" --input="$DIR/core-fonts" --allfonts-web="$DIR/sdkjs/common/AllFonts.js" --allfonts="$DIR/server/FileConverter/bin/AllFonts.js" --images="$DIR/sdkjs/common/Images" --selection="$DIR/server/FileConverter/bin/font_selection.bin" --output-web="$DIR/fonts" --use-system="true"

echo "=> Initialize and migrate database"
export PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD}
psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -f "$DIR/server/schema/postgresql/createdb.sql"
# Some versions need a database migration
# those are in /var/www/onlyoffice/documentserver/server/schema/postgresql/upgrade last one was for 7.2.0
# psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -f "$DIR/server/schema/postgresql/upgrade/upgradev720.sql"

# delete old rabbitmq (otherwise, we have to handle upgrades)
rm -rf /app/data/rabbitmq && mkdir -p /app/data/rabbitmq
echo "=> Start rabbitmq server"
chown -R rabbitmq:rabbitmq /app/data/rabbitmq /tmp/rabbitmq
if [[ -f /var/lib/rabbitmq/.erlang.cookie ]]; then
    chmod 600 /var/lib/rabbitmq/.erlang.cookie
fi

echo "=> Fixup permissions"
chown -R ds:ds /app/data/onlyoffice

echo "=> Start ONLYOFFICE daemons"
exec /usr/bin/supervisord --nodaemon --configuration /etc/supervisor/supervisord.conf
