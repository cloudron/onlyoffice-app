#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const CUBBY_LOCATION = LOCATION + '-companion';
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 100000;
    const EXEC_ARGS = { stdio: 'inherit' };

    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;

    let browser, app, cubbyApp;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo(appLocation) {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        const app = inspect.apps.filter(function (a) { return a.location === appLocation || a.location === `${appLocation}2`; })[0];
        expect(app).to.be.an('object');
        return app;
    }

    function getCubbyAppInfo() {
        cubbyApp = getAppInfo(CUBBY_LOCATION);
    }

    function getOOAppInfo() {
        app = getAppInfo(LOCATION);
    }

    async function cubbyLogin() {
        await browser.manage().deleteAllCookies();
        await browser.get(`https://${cubbyApp.fqdn}`);

        await waitForElement(By.id('loginButton'));
        await browser.findElement(By.id('loginButton')).click();

        await waitForElement(By.id('inputUsername'));
        await browser.findElement(By.id('inputUsername')).sendKeys(username);
        await browser.findElement(By.id('inputPassword')).sendKeys(password);
        await browser.findElement(By.id('loginSubmitButton')).click();

        await waitForElement(By.id('profileMenuDropdown'));
    }

    async function cubbyEnableOnlyOfficeApp() {
        await browser.get(`https://${cubbyApp.fqdn}/#settings`);

        await waitForElement(By.id('wopiHostnameInput'));
        await browser.findElement(By.id('wopiHostnameInput')).clear();
        await browser.findElement(By.id('wopiHostnameInput')).sendKeys(`https://${app.fqdn}`);
        await browser.findElement(By.id('wopiHostnameInput')).sendKeys(Key.ENTER);

        await browser.sleep(2000);
    }

    function cubbyUploadTestFile() {
        execSync(`cloudron push --app ${cubbyApp.fqdn} ./test.odt /app/data/data/${username}/test.odt`, EXEC_ARGS);
    }

    async function cubbyOpenEditor() {
        await browser.get('about:blank');
        await browser.get(`https://${cubbyApp.fqdn}/#files/home/test.odt`); // opens a new window
        await browser.sleep(5000);

        const originalWindowHandle = await browser.getWindowHandle();
        const allWindowHandles = await browser.getAllWindowHandles();
        const newWindowHandle = allWindowHandles.filter((h) => h !== originalWindowHandle).join();

        await browser.switchTo().window(newWindowHandle);

        await waitForElement(By.xpath('//iframe[@name="document-viewer"]'));
        const appFrame = await browser.findElement(By.xpath('//iframe[@name="document-viewer"]'));
        await browser.switchTo().frame(appFrame);

        await waitForElement(By.xpath('//iframe[@name="frameEditor"]'));
        const editorFrame = await browser.findElement(By.xpath('//iframe[@name="frameEditor"]'));
        await browser.switchTo().frame(editorFrame);

        await waitForElement(By.xpath('//a[contains(., "File")]'));

        await browser.close(); // close window
        await browser.switchTo().window(originalWindowHandle);
        await browser.switchTo().defaultContent();
    }

    async function welcomePage() {
        await browser.get(`https://${app.fqdn}/welcome/`);
        await browser.sleep(4000);

        await waitForElement(By.xpath('//div[contains(text(), "Document Server is running")]'));
    }

    async function openEditor() {
        await browser.get(`https://${app.fqdn}/web-apps/apps/documenteditor/main/index.html`);
        await browser.sleep(4000);

        await waitForElement(By.xpath('//*[@id="loading-mask"]'));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getOOAppInfo);

    it('can open welcome page', welcomePage);
    it('can open editor (only loading page)', openEditor);

    it('install Cubby', async function () { execSync(`cloudron install --appstore-id io.cloudron.cubby --location ${CUBBY_LOCATION}`, EXEC_ARGS); });
    it('can get Cubby app information', async function () { getCubbyAppInfo(); });

    it('can login to Cubby', cubbyLogin);
    it('can enable OnlyOffice app in Cubby', cubbyEnableOnlyOfficeApp);
    it('can upload file to Cubby', cubbyUploadTestFile);

    it('can open editor in Cubby', cubbyOpenEditor);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });

    it('can open welcome page', welcomePage);
    it('can open editor (only loading page)', openEditor);

    it('can open editor in Cubby', cubbyOpenEditor);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', async function () {
        await browser.get('about:blank'); // ensure we don't hit NXDOMAIN in the mean time

        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getOOAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can open welcome page', welcomePage);
    it('can open editor (only loading page)', openEditor);

    it('can open editor in Cubby', cubbyOpenEditor);

    it('move to different location', async function () {
        await browser.get('about:blank'); // ensure we don't hit NXDOMAIN in the mean time
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
    });

    it('can get app information', getOOAppInfo);

    it('can open welcome page', welcomePage);
    it('can open editor (only loading page)', openEditor);

    it('can enable OnlyOffice app in Cubby', cubbyEnableOnlyOfficeApp);
    it('can open editor in Cubby', cubbyOpenEditor);

    it('uninstall app', async function () {
        await browser.get('about:blank'); // ensure we don't hit NXDOMAIN in the mean time
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // test update
    it('can install app', function () { execSync(`cloudron install --appstore-id com.onlyoffice.coudronapp --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getOOAppInfo);
    it('can open welcome page', welcomePage);
    it('can open editor (only loading page)', openEditor);

    it('can enable OnlyOffice app in Cubby', cubbyEnableOnlyOfficeApp);
    it('can open editor in Cubby', cubbyOpenEditor);

    it('can update', function () { execSync(`cloudron update --app ${LOCATION}`, EXEC_ARGS); });

    it('can open welcome page', welcomePage);
    it('can open editor (only loading page)', openEditor);

    it('can open editor in Cubby', cubbyOpenEditor);

    it('uninstall app', async function () {
        await browser.get('about:blank'); // ensure we don't hit NXDOMAIN in the mean time
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    it('uninstall OnlyOffice', function () {
        execSync(`cloudron uninstall --app ${CUBBY_LOCATION}`, EXEC_ARGS);
    });
});
